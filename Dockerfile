FROM openjdk:8
EXPOSE 8080
ADD target/mypro.jar mypro.jar
ENTRYPOINT ["java","-jar","mypro.jar"]
